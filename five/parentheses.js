
DifferenceCalculator.filter('parentheses', parentheses);

function parentheses(){
    return function(input){
        var output;

        if(input<0){
            input = input*(-1);
            output = '('+input+')';
        }else{
            output = input;
        }

        return output;
    };
}
