var DifferenceCalculator = angular.module('DifferenceCalculator', []);

DifferenceCalculator.directive('calculator', calculator);

function calculator(){
    return{
        scope: {},
        restrict: 'E',
        template:   '<div>' +
                        '<input ng-model="touple.number1" style="width: 50px"/> - <input ng-model="touple.number2" style="width: 50px"/>' +
                        '<button ng-click="subtract()"> = </button> {{result}}' +
                    '</div>',
        controller: function ($scope, historyFactory){
            var touple = {
                number1: 1,
                number2: 1
            };

            $scope.touple = touple;

            $scope.subtract = function(){
                var result = touple.number1 - touple.number2;
                $scope.result = result;

                historyFactory.add({num1: touple.number1, num2: touple.number2, res: result});
            }
        }
    };
}