var PivotTable = angular.module('PivotTable', ['PivotData']);

PivotTable.directive('pivottable', pivottable);

function pivottable(){
    return{
        scope: {},
        restrict: 'E',
        templateUrl: 'pivot_table_template.html',
        controller: [
		'$scope',
		'data',
		function($scope, data){
            $scope.colAttributes = data.getColAttributes();
            $scope.colValues = data.getColValues();
            $scope.colValueSpans = data.getColValueSpans();

            $scope.rowAttributes = data.getRowAttributes();
            $scope.rowValueSpans = data.getRowValueSpans();

            $scope.rowData = data.getRowData();   //contains rowValues too

            $scope.rowTotals = data.getRowTotals();
        }]
    };
}

