var RawDataProvider = angular.module('RawDataProvider', []);

RawDataProvider.factory('rawDataProvider', rawDataProvider);

function rawDataProvider(){
    var rawData = [
        ['a3', 'a4', 'a1', 'a2'],

        ['a3v1', 'a4v1', 'a1v1', 'a2v1', 5],
        ['a3v1', 'a4v1', 'a1v2', 'a2v3', 1],
        ['a3v1', 'a4v1', 'a1v2', 'a2v5', 2],
        ['a3v1', 'a4v1', 'a1v2', 'a2v1', 3],

        ['a3v2', 'a4v2', 'a1v1', 'a2v1', 6],
        ['a3v2', 'a4v2', 'a1v2', 'a2v3', 2],
        ['a3v2', 'a4v2', 'a1v2', 'a2v5', 2],
        ['a3v2', 'a4v2', 'a1v2', 'a2v1', 4],

        ['a3v2', 'a4v5', 'a1v1', 'a2v1', 9],
        ['a3v2', 'a4v5', 'a1v2', 'a2v3', 5],
        ['a3v2', 'a4v5', 'a1v2', 'a2v5', 5],
        ['a3v2', 'a4v5', 'a1v2', 'a2v1', 7]
    ];

    return rawData;
}