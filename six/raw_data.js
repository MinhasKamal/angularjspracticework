var RawData = angular.module('RawData', ['RawDataProvider']);

RawData.factory('rawData', rawData);

function rawData(){
    var rawData = rawDataProvider();
    var columnRowDivider = 2;

    var colAttributes = getColAttributes(rawData, columnRowDivider);//console.log(colAttributes);
    var rowAttributes = getRowAttributes(rawData, columnRowDivider);//console.log(rowAttributes);

    var valuesAndTotals = valuesAndTotalsProcessor(rawData, columnRowDivider);
    var colValues = valuesAndTotals.getColValues();
    var rowValues = valuesAndTotals.getRowValues();
    var totals = valuesAndTotals.getTotals();

    var rowTotals = getRowTotals();

    var colTotals = getColTotals();

    //var pData = [];

    return getPivotData();
}

function getColAttributes(rawData, columnRowDivider){
    var colAttributes=[];
    for(var i=0; i<columnRowDivider; i++){
        colAttributes.push(rawData[0][i]);
    }
    return colAttributes;
}
function getRowAttributes(rawData, columnRowDivider){
    var rowAttributes=[];
    for(var i=columnRowDivider; i<rawData[0].length; i++){
        rowAttributes.push(rawData[0][i]);
    }
    return rowAttributes;
}

function valuesAndTotalsProcessor(rawData, columnRowDivider){
    var colValues = [];
    var rowValues = [];
    var totals = [];

    var rowNo=1;

    var colValueArray=[];
    var rowValueArray=[];
    var total;
    for(var i=0; i<columnRowDivider; i++){
        colValueArray.push(rawData[rowNo][i]);
    }
    for(var i=columnRowDivider; i<rawData[rowNo].length-1; i++){
        rowValueArray.push(rawData[rowNo][i]);
    }
    total = rawData[rowNo][rawData[rowNo].length-1];
    //console.log(colValueArray);
    //console.log(rowValueArray);
    //console.log(total);

    var colPosition = insertElement(colValues, colValueArray);
    var rowPosition = insertElement(rowValues, rowValueArray);

    //return colAttributes;
    //return rowAttributes;

    return {
        getColValues: function(){
            return colValues;
        },
        getRowValues: function(){
            return rowValues;
        },
        getTotals: function(){
            return totals;
        }
    };
}

function insertElement(list, element){
    var position = -1;

    for(var i=0; i<list.length; i++){
        var matched = true;
        for(var j=0; j<list[i].length; j++){
            if(list[i][j] != element[j]){
                matched = false;
                break;
            }
        }

        if(matched){
            position = i;
            break;
        }
    }

    if(position<0){
        position = list.length;
        list.push(element);
    }

    return position;
}

function getRowTotals(){
    return [];
}

function getColTotals(){
    return [];
}

function getPivotData(){
    //var pData = [
    //    ['a3', 'a4', 'a5'],
    //    ['a1', 'a2', 'a6'],
    //    [ ['a3v1', 'a4v1', 'a5v1'], ['a3v1', 'a4v2', 'a5v5'], ['a3v2', 'a4v3', 'a5v1'],
    //        ['a3v2', 'a4v3', 'a5v2'], ['a3v2', 'a4v5', 'a5v1'] ],
    //    [ ['a1v1', 'a2v9', 'a6v1'], 5, 6, 7, 8, 9, 100 ],
    //    [ ['a1v1', 'a2v2', 'a6v2'], 5, 6, 7, 8, 9, 100 ],
    //    [ ['a1v2', 'a2v3', 'a6v3'], 13, 2, 30, 4, 5, 100 ],
    //    [ ['a1v2', 'a2v3', 'a6v4'], 1, 2, 3, 4, 5, 100 ],
    //    [ ['a1v2', 'a2v2', 'a6v5'], 6, 2, 37, 4, 9, 100 ],
    //    [ ['a1v3', 'a2v6', 'a6v6'], 1, 2, 3, 4, 5, 100 ],
    //    [ ['a1v4', 'a2v7', 'a6v7'], 1, 2, 3, 4, 5, 100 ],
    //    [ ['a1v4', 'a2v7', 'a6v8'], 15, 0, 36, 4, 5, 100 ],
    //    [ ['a1v4', 'a2v6', 'a6v9'], 1, 2, 3, 45, 6, 100 ],
    //    [ ['a1v4', 'a2v1', 'a6v4'], 38, 4, 5, 6, 7, 100 ],
    //    [ 100, 100, 100, 100, 100, 999 ]
    //];

    //var pData = [
    //    [],
    //    ['a1', 'a2'],
    //    [[]],
    //    [ ['a1v1', 'a2v1'], 35 ],
    //    [ ['a1v1', 'a2v8'], 15 ],
    //    [ ['a1v1', 'a2v4'], 15 ],
    //    [ ['a1v2', 'a2v3'], 15 ],
    //    [ ['a1v2', 'a2v5'], 15 ],
    //    [ ['a1v2', 'a2v6'], 15 ],
    //    [ ['a1v2', 'a2v1'], 25 ],
    //    [ 135 ]
    //];

    //var pData = [
    //    [],
    //    [],
    //    [[]],
    //    [[]],
    //    [ 135 ]
    //];

    //var pData = [
    //    ['a3', 'a4'],
    //    [],
    //    [ ['a3v1', 'a4v1'], ['a3v1', 'a4v2'], ['a3v2', 'a4v3'], ['a3v2', 'a4v1'], ['a3v2', 'a4v5'] ],
    //    [[]],
    //    [ 13, 20, 27, 34, 41, 135 ]
    //];

    //var pData = [
    //    ['a3', 'a4'],
    //    ['a1', 'a2'],
    //    [ ['a3v1', 'a4v1'], ['a3v1', 'a4v2'], ['a3v2', 'a4v3'], ['a3v2', 'a4v1'], ['a3v2', 'a4v5'] ],
    //    [ ['a1v1', 'a2v1'], 5, 6, 7, 8, 9, 35 ],
    //    [ ['a1v1', 'a2v8'], 1, 2, 3, 4, 5, 15 ],
    //    [ ['a1v1', 'a2v4'], 1, 2, 3, 4, 5, 15 ],
    //    [ ['a1v2', 'a2v3'], 1, 2, 3, 4, 5, 15 ],
    //    [ ['a1v2', 'a2v5'], 1, 2, 3, 4, 5, 15 ],
    //    [ ['a1v2', 'a2v6'], 1, 2, 3, 4, 5, 15 ],
    //    [ ['a1v2', 'a2v1'], 3, 4, 5, 6, 7, 25 ],
    //    [ 13, 20, 27, 34, 41, 135 ]
    //];

    var pData = [
        ['a3', 'a4'],
        ['a1', 'a2'],
        [ ['a3v1', 'a4v1'], ['a3v2', 'a4v2'], ['a3v2', 'a4v5'] ],
        [ ['a1v1', 'a2v1'], 5, 6, 9, 50 ],
        [ ['a1v2', 'a2v3'], 1, 2, 5, 50 ],
        [ ['a1v2', 'a2v5'], 1, 2, 5, 50 ],
        [ ['a1v2', 'a2v1'], 3, 4, 7, 50 ],
        [ 50, 50, 50, 555 ]
    ];

    return pData;
}
