var PivotData = angular.module('PivotData', ['RawData']);

PivotData.factory('data', data);

function data(){
    var pivotData = rawData();

    var colValuesAndSpans = getColValuesAndSpans(pivotData[2]);
    var colValues = colValuesAndSpans.getColValues();
    var colValueSpans = colValuesAndSpans.getColValueSpans();

    var rowDataAndSpans = getRowDataAndSpans(pivotData.slice(3, pivotData.length-1));
    var rowData = rowDataAndSpans.getRowData();
    var rowValueSpans = rowDataAndSpans.getRowValueSpans();

    return {
        getAll: function(){
            return pivotData;
        },

        getColAttributes: function(){
            return pivotData[0];
        },

        getColValues: function(){
            return colValues;
        },

        getColValueSpans: function(){
            return colValueSpans
        },

        getRowAttributes: function(){
            return pivotData[1];
        },

        getRowValueSpans: function(){
            return rowValueSpans
        },

        getRowData: function(){
            return rowData;
        },

        getRowTotals: function(){
            return pivotData[pivotData.length-1];
        }
    };
}

function getColValuesAndSpans(colArrays){
    var colValues = [];
    var colValueSpans = [];

    for(var i=0; i<colArrays[0].length; i++){
        var colValue = [];
        var colValueSpan = [];
        colValue.push(colArrays[0][i]);
        colValueSpan.push(1);
        for(var j=1; j<colArrays.length; j++){
            if(colValue[colValue.length-1] != colArrays[j][i]){
                colValue.push(colArrays[j][i]);
                colValueSpan.push(1);
            }else{
                colValueSpan[colValueSpan.length-1]++;
            }
        }

        colValueSpans.push(colValueSpan);
        colValues.push(colValue);
    }

    return{
        getColValues: function(){
            return colValues;
        },

        getColValueSpans: function(){
            return colValueSpans
        }
    };
}

function getRowDataAndSpans(rowData){
    var rowValueSpans = [];
    var rowDataDeleteIndexes = [];
    for(var i=0; i<rowData.length; i++){
        rowValueSpans.push([]);
    }

    for(var i=0; i<rowData[0][0].length; i++){
        var rowValue = [];
        var rowValueSpan = [];
        rowValue.push(rowData[0][0][i]);
        rowValueSpan.push(1);

        for(var j=1; j<rowData.length; j++){
            if(rowValue[rowValue.length-1] != rowData[j][0][i]){
                rowValue.push(rowData[j][0][i]);
                rowValueSpan.push(1);
            }else{
                rowValueSpan[rowValueSpan.length-1]++;
                //rowData[j][0].splice(i, 1);   //done later
                rowDataDeleteIndexes.push([j, i]);
            }
        }

        var index=0;
        for(var j=0; j<rowValueSpan.length; j++){
            rowValueSpans[index].push(rowValueSpan[j]);
            index += rowValueSpan[j];
        }
    }

    for(var i=rowDataDeleteIndexes.length-1; i>=0; i--){ //rowData[j][0].splice(i, 1);
        rowData[ rowDataDeleteIndexes[i][0] ][0].splice(rowDataDeleteIndexes[i][1], 1);
    }

    return{
        getRowData: function(){
            return rowData;
        },

        getRowValueSpans: function(){
            return rowValueSpans
        }
    };
}