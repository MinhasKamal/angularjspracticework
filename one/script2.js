var Car = function(brand, horsePower){
    this.brand = brand;
    this.horsePower = horsePower;
}

function printCar(car){
    console.log(car);
}

var z8 = new Car('Lamborghini', 120);
printCar(z8);