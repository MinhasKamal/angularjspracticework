var TableLoader = angular.module('TableLoader', ['DataConnector']);

TableLoader.directive('table', tableLoaderDirective);

function tableLoaderDirective(){
    return {
        restrict: 'E',
        templateUrl: 'table.html'
    };
}