var DataConnector = angular.module('DataConnector', []);

DataConnector.controller("userDataHeadCtrl", function($scope){
    var heads = {
        head1: "Image",
        head2: "First Name",
        head3: "Last Name",
        head4: "Age",
        head5: "Type",
        head6: "Ratings",
        head7: "upVote",
        head8: "downVote"
    };

    $scope.heads = heads;
});

DataConnector.controller("userDataCtrl", function($scope){
    var users = [
        {firstName: "Minhas1", lastName: "Kamal1", age: "20", type: "very low", img: "user.png", ratings: "0"},
        {firstName: "Minhas2", lastName: "Kamal2", age: "21", type: "low", img: "user.png", ratings: "0"},
        {firstName: "Minhas3", lastName: "Kamal3", age: "22", type: "normal", img: "user.png", ratings: "0"},
        {firstName: "Minhas4", lastName: "Kamal4", age: "23", type: "high", img: "user.png", ratings: "0"},
        {firstName: "Minhas5", lastName: "Kamal5", age: "24", type: "very high", img: "user.png", ratings: "0"},
        {firstName: "Minhas6", lastName: "Kamal6", age: "25", type: "star", img: "user.png", ratings: "0"}
    ];

    $scope.users = users;

    $scope.incrementRating = function(user){
        user.ratings++;
    }

    $scope.decrementRating = function(user){
        user.ratings--;
    }
});