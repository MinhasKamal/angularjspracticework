var TreeTable = angular.module('TreeTable', ['Tree']);

TreeTable.directive('treetable', treetable);

function treetable(){
    return{
        scope: {
            node: '='
        },
        restrict: 'E',
        templateUrl: 'tree_table_template.html',
        controller: function($scope){
            console.log($scope.node);
        }
    };
}