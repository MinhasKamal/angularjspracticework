function Node(name){
    this.name = name;
    this.value = null;
    this.parent = null;
    this.children = [];
}

Node.prototype.addChild = function(child){
    this.children.push(child);
    child.parent = this;
};

Node.prototype.setValue = function(value){
    this.value = value;
};

Node.prototype.toString= function(){
    var fullName = "["+this.name;

    if(this.value!=null){
        fullName += ":"+this.value;
    }

    for(var i in this.children){
        fullName += this.children[i].toString()+" ";
    }

    return fullName+"]";
};
