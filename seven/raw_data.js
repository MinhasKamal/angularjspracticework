function Data(){
    this.jsonData = {
        "a3v1":{
            "a4v1":{
                "a1v1":{
                    "a2v1": 5
                },
                "a1v2":{
                    "a2v3": 1,
                    "a2v5": 1,
                    "a2v1": 3
                }
            }
        },
        "a3v2":{
            "a4v2":{
                "a1v1":{
                    "a2v1": 6
                },
                "a1v2":{
                    "a2v3": 2,
                    "a2v5": 2,
                    "a2v1": 4
                }
            },
            "a4v5":{
                "a1v1":{
                    "a2v1": 9
                },
                "a1v2":{
                    "a2v3": 5,
                    "a2v5": 5,
                    "a2v1": 7
                }
            }
        }
    };

    this.attributes = {
        "colAttributes": ['a3', 'a4'],
        "rowAttributes": ['a1', 'a2']
    };
}

Data.prototype.getJsonData = function(){
    return this.jsonData;
};

Data.prototype.getColAttributes = function(){
    return this.attributes["colAttributes"];
};

Data.prototype.getRowAttributes = function(){
    return this.attributes["rowAttributes"];
};
