var Tree = angular.module('Tree', []);

Tree.factory('tree', tree);
function tree(){
    var rawdata = new Data();
    var root = treeBuilder(rawdata.getJsonData());
    //console.log(root.toString());

    return {
        getTreeRoot: function(){
            return root;
        },

        getColAttributes: function(){
            return rawdata.getColAttributes();
        },

        getRowAttributes: function(){
            return rawdata.getRowAttributes();
        }
    };
}

function treeBuilder(pivotData){
    var root = new Node("pivotData");
    createTree(root, pivotData);
    return root;
}

function createTree(parentNode, data){
    var noOfChild = Object.keys(data).length;
    if(noOfChild==0){
        parentNode.setValue(data);
    }else{
        for(var d in data){
            var childData = data[d];
            var childNode = new Node(d);
            parentNode.addChild(childNode);
            createTree(childNode, childData);
        }
    }
}


//function Tree(){
//    this.columnTree = {
//        "a3v1":{
//            "a4v1":[],
//            "a4v4":[]
//        },
//        "a3v2":{
//            "a4v1":[],
//            "a4v3":[]
//        }
//    };
//
//    this.rowTree = {
//        "a1v1":{
//            "a2v1":[]
//        },
//        "a1v3":{
//            "a2v1":[],
//            "a2v3":[],
//            "a2v4":[]
//        }
//    };
//}
