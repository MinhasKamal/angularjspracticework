var Calculator = angular.module('Calculator', ['MathFactory', 'History']);

Calculator.directive('calculator', calculator);

function calculator(){
    return{
        scope: {},
        restrict: 'E',
        template:   '<div>' +
                        '<input ng-model="touple.number1" style="width:50px"/>' +
                        '<button style="width:50px" ng-click="changeSign()">{{touple.sign}}</button>' +
                        '<input ng-model="touple.number2" style="width:50px"/>' +
                        '<button ng-click="calc()" style="width:50px"> = </button>' +
                        '{{result}}' +
                    '</div>' +
                    '<div style="overflow-y:scroll; height:80px;">' +
                    '<div ng-repeat="toup in touples">' +
                        '{{toup.num1}} {{toup.sgn}} {{toup.num2}} = {{toup.res}}' +
                    '</div>' +
                    '</div>',
        controller: function($scope, mathServices, historyFactory){
            var touple = {
                sign: '+',
                number1: 1,
                number2: 2
            };

            $scope.touple = touple;

            $scope.touples = historyFactory.getAll();

            $scope.calc = function(){
                var result = mathServices.calculate(touple.sign, touple.number1, touple.number2);
                $scope.result = result;

                historyFactory.add({num1: touple.number1, sgn: touple.sign, num2: touple.number2, res: result});
            }

            $scope.changeSign = function(){
                if(touple.sign == '+'){
                    touple.sign = '-';
                }else if(touple.sign == '-'){
                    touple.sign = '*';
                }else if(touple.sign == '*'){
                    touple.sign = '/';
                }else if(touple.sign == '/'){
                    touple.sign = '%';
                }else if(touple.sign == '%'){
                    touple.sign = '+';
                }
            }
        }
    };
}

