var MathFactory = angular.module('MathFactory', []);

MathFactory.factory('mathServices', mathServices);

function mathServices(){
    var mathUtils = {};

    mathUtils.add = function(a, b){
        return a+b;
    };

    mathUtils.subtract = function(a, b){
        return a-b;
    };

    mathUtils.multiply = function(a, b){
        return a*b;
    };

    mathUtils.divide = function(a, b){
        return a/b;
    };

    mathUtils.modulo = function(a, b){
        return a%b;
    };

    mathUtils.calculate = function(sign, number1, number2){
        if(sign=='+'){
            return this.add(number1, number2);
        }else if(sign=='-'){
            return this.subtract(number1, number2);
        }else if(sign=='*'){
            return this.multiply(number1, number2);
        }else if(sign=='/'){
            return this.divide(number1, number2);
        }else if(sign=='%'){
            return this.modulo(number1, number2);
        }
    }

    return mathUtils;
}