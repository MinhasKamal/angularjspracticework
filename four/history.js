var History = angular.module('History', []);

History.factory('historyFactory', historyFactory);

function historyFactory(){

    var touples = [];
    return {
        add: function(touple){
            touples.push(touple);
        },
        getAll: function(){
            return touples;
        }
    };
}
